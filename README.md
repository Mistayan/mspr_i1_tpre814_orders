# Application multi-services : Order-Manager

Auteur :
Mistayan - [Gitlab](https://gitlab.com/Mistayan/)

## Description

Ce projet est un exemple d'application multi-services utilisant Spring Boot et Spring Cloud. Il est composé de 3
services :

- order-service : Service de gestion des commandes
- product-service : Service de gestion des produits
- customer-service : Service de gestion des clients

Ainsi que d'un Broker, permettant la communication entre les services.

- Kafka : Broker de messageries AVANCE

## Technologies

- Java 21
- Spring Boot 3.2.4
- Postgresql
- JPA

## Installation

### Prérequis

- Java 21
- Gradle 8.5+
- Docker
- Docker-compose

[//]: # (- Postgresql)

### Installation

1. Cloner le projet
    ```shell
    git clone https://gitlab.com/Mistayan/order-manager.git 
    ```
2. Se rendre dans le dossier du projet
    ```shell
    cd order-manager
    ```

3. Lancer la commande `./graldew bootBuildImage` pour construire le programme en Image:
    ```shell
    ./gradlew bootBuildImage
    ```
4. puis `docker compose up -d` pour lancer les conteneurs docker
    ```shell
    docker compose up -d
   ```
5. L'API est accessibles à l'adresse `http://localhost:30045`

6. Pour arrêter les conteneurs, lancer la commande `docker compose down`
    ```shell
    docker compose down
    ```
7. Pour supprimer les images récupérées, lancer la commande :
    ```shell
      docker rmi order-service postgres:15
    ```
## Utilisation

### Application globale

<table>
  <tr>
    <th>Service</th>
    <th>Port</th>
    <th>URL</th>
  </tr>
  <tr>
    <td>customer-service</td>
    <td>30050</td>
    <td>http://localhost:30050</td>
  </tr>
 <tr>
    <td>product-service</td>
    <td>30055</td>
    <td>http://localhost:30055</td>
  </tr>
  <tr>
    <td>order-service</td>
    <td>30045</td>
    <td>http://localhost:30045</td>
  </tr>

</table>


____________________________________________
____

### Customer-service

____

#### Créer un client

- URL : `http://localhost:30050/customers`
- Méthode : POST
- Body :

```json
{
  "username": "Bobbie_Dickens",
  "address": {
    "postalCode": "48348",
    "city": "East Toniton"
  },
  "profile": {
    "firstName": "Bradford",
    "lastName": "Kuphal"
  },
  "company": {
    "companyName": "Fritsch, Bayer and Sanford"
  }
}
```
      - Optionnel à la création du compte: `company`, `address`
      - Obligatoires pour commander : `username`, `profile`, `address`, `company`

- Réponse : 
```json
{
  "id": 13,
  "name": "Bridget Torp"
}
```

#### Récupérer un client

- URL : `http://localhost:30050/customers/{id}`
- Méthode : GET
- Paramètre : id
- Exemple : `http://localhost:30050/customers/13`
- Réponse :

```json
{
   "id": "13",
   "createdAt": "2023-08-30T13:26:04.774Z",
   "username": "Bobbie_Dickens",
   "address": {
      "postalCode": "48348",
      "city": "East Toniton"
   },
   "profile": {
      "firstName": "Bradford",
      "lastName": "Kuphal"
   },
   "company": {
      "companyName": "Fritsch, Bayer and Sanford"
   },
   "orders": [
      {
         "createdAt": "2023-08-29T21:22:06.346Z",
         "id": "5",
         "customerId": "13"
      }
   ]
}
```

____________________________________________
____

### Product-service

____

#### Créer un produit

- URL: `http: //localhost:30055/products`
- Méthode: POST
- Body: 
```json
{
  "name": "Example product",
  "details": {
    "price": "662.00",
    "description": "Any description here",
    "color": "Any color here"
  },
  "stock": 132
}
```

#### Récupérer un produit

- URL : `http://localhost:30055/products/{id}`
- Méthode : GET
- Paramètre : id
- Exemple : `http://localhost:30055/products/11`
- Réponse :

```json
{
   "createdAt": "2023-08-30T10:53:17.204Z",
   "name": "Ernestine Willms",
   "details": {
      "price": "662.00",
      "description": "The Football Is Good For Training And Recreational Purposes",
      "color": "mint green"
   },
   "stock": 92876,
   "id": "11"
}
```

____________________________________________

____

### Order-service

____

#### Créer une commande

- URL : `http://localhost:30045/orders`
- Méthode : POST
- Body :

```json
{
  "customerId": "11",
  "products": [
    "..."
  ]
}
```

#### Récupérer une commande

- URL : `http://localhost:30045/orders/{id}`
- Méthode : GET
- Paramètre : id
- Exemple : `http://localhost:30045/orders/3`
- Réponse :

```json
{
  "createdAt": "2023-08-30T08:35:27.416Z",
  "id": "3",
  "customerId": "11",
  "products": [
    {
      "createdAt": "2023-08-30T03:16:28.440Z",
      "name": "Rufus Pacocha",
      "details": {
        "price": "97.00",
        "description": "The Football Is Good For Training And Recreational Purposes",
        "color": "red"
      },
      "stock": 73253,
      "id": "3",
      "orderId": "3"
    },
    {
      "createdAt": "2023-08-29T22:55:14.728Z",
      "name": "Paula Hodkiewicz",
      "details": {
        "price": "139.00",
        "description": "The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients",
        "color": "salmon"
      },
      "stock": 1609,
      "id": "53",
      "orderId": "3"
    }
  ]
}
```

## Exemples d'utilisation

____________________________________________

Un Employé inscrit un nouveau client dans l'ERP

```mermaid
sequenceDiagram
   participant Front
   participant ClientService
   participant Client DB
   Front ->> ClientService: Créer un client
   ClientService ->> Client DB: Enregistrer le client
   Client DB -->> ClientService: Client enregistré
   ClientService -->> Front: Client enregistré
```

____________________________________________

Un Employé inscrit des nouveaux produits dans l'ERP

```mermaid
sequenceDiagram
   participant Front
   participant ProductService
   participant Product DB
   Front ->> ProductService: Créer un produit
   ProductService ->> Product DB: Enregistrer le produit
   ProductService -->> Front: Produit enregistré
   Front ->> ProductService: Créer un produit
   ProductService ->> Product DB: Enregistrer le produit
   ProductService -->> Front: Produit enregistré

```

____________________________________________

Un Employé crée une commande pour un client
```mermaid
sequenceDiagram
    participant Front
    participant OrderService
    participant Order DB
    Front ->> OrderService: {customerId: 1, products: [{id: 1, qty:1}, {id: 2, qty:1}, {id: 3, qty:1}]}
    OrderService ->> Order DB: Enregistrer la commande
    Order DB -->> OrderService: Order{id: 1, ...}
    OrderService -->> OrderService: Vérifications
# Si un service est en erreur, on annule la commande
    critical fail
        OrderService ->> Order DB: Annuler la commande
        OrderService -->> Front: Commande annulée Error{reason: "Aucun produit demandé en stock"}
    end
    alt OK
        OrderService -->> Front: Commande enregistrée Order{id: 1, ...}
    end 
```

______
______
# Fonctionnement interne, en incluant le Broker Kafka

Utilisant une BDD par service, il est conseillé d'utiliser le
pattern [SAGA](https://microservices.io/patterns/data/saga.html) :

- https://microservices.io/patterns/data/database-per-service.html

OrderService porte la responsabilité de la transaction, et doit être capable d'annuler la commande si un service n'a pas
pu valider la commande.
Il devra aussi propager les erreurs aux services concernés. (gestion native avec Kafka-Spring: @Transactional(
propagation = Propagation.REQUIRED))
___

### 0/ L'utilisateur crée une nouvelle commande pour le client 1, incluant les articles 1, 2 et 3

Il faut initier une `transaction` par service effectuant un `Process` recevant des instructions, afin de pouvoir revenir
en arrière si l'un des services n'a pas pu valider la commande.
____

### 1/ On crée une `commande temporaire`, pour réserver les articles en stock et l'associer au client.

```mermaid
sequenceDiagram
   OrderService ->> Order DB: Créer une commande temporaire
   Order DB ->> OrderService: Commande enregistrée {id: 1, ...}
```

L'`orderId` nous sert d'`id de transaction`, pour pouvoir annuler la commande si un service n'a pas pu valider la
commande.
____

### 2/ On doit vérifier la disponibilité des `articles en stock`, et renseigner la `commande` chez le `client`.

- à l'avenir, il est aussi possible de réduire le portefeuille client

#### __Enregistrement de la commande du Customer__ :

- Topic : customer-request : `CustomerRequest{orderId, customerId}`
- Topic : customer-response : `CustomerResponse{orderId, Customer{id: 1, ...}}`

```mermaid
sequenceDiagram
   OrderService -->> Broker: CustomerRequest {key, customerId: 1}
   CustomerService ->> Broker: Listen CustomerRequest
   CustomerService ->> CustomerService: Trouver le client et ajouter la commande
   CustomerService -->> Broker: Response
   Broker ->> OrderService: CustomerResponse {key, Customer{id: 1, ...}}
```

____

#### __Réservation des produits__ :

- Topic: stock-request : `StockRequest{key, ProductRequest[]: [{id, qty(Wanted)}, ...], StockOperation}`
- Topic: stock-response : `StockResponse{key, Product[]: [{id, qty(Available), unit_price}, ...]}`

```mermaid
sequenceDiagram
   OrderService ->> Broker: StockRequest {orderId, [{id:1, qty:3} , ...], op:SUB}
   Broker -->> ProductService: StockRequest
   ProductService -> ProductService: Trouver les produits, Décrémenter le stock
   ProductService ->> Broker: StockResponse
   Broker -->> OrderService: StockResponse {orderId, products: [{id:1, qty:512, ...} , ...]}
``` 

______


## Licence

Ce projet est sous licence MIT - voir le fichier [LICENSE.md](LICENSE.md) pour plus d'informations

## Contacts

Mistayan - [Gitlab](https://gitlab.com/Mistayan/) 
Bob-117 - [Gitlab](https://gitlab.com/Bob-117/)
Sullfurick - [Gitlab](https://gitlab.com/Sullfurick)