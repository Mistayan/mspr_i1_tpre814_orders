package fr.epsi.rennes.mspr.tpre814.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ContextConfiguration(classes = {KafkaConfig.class})
@ExtendWith(SpringExtension.class)
class KafkaConfigDiffblueTest {
    /**
     * Method under test: {@link KafkaConfig#orderTopic()}
     */
    @Test
    void testOrderTopic() {
        //   Diffblue Cover was unable to create a Spring-specific test for this Spring method.

        // Arrange and Act
        NewTopic actualOrderTopicResult = (new KafkaConfig()).orderTopic();

        // Assert
        assertNull(actualOrderTopicResult.replicasAssignments());
        assertNull(actualOrderTopicResult.configs());
        assertEquals(KafkaConfig.ORDER_TOPIC, actualOrderTopicResult.name());
    }
}
