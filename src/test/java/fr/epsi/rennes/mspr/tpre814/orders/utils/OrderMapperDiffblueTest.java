package fr.epsi.rennes.mspr.tpre814.orders.utils;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class OrderMapperDiffblueTest {
    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder2() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(null);
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder3() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(null);
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder4() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Updating order {}\n with {}");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(orderItems);
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder5() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(null);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder6() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(0.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals("Warn", dbOrder.getWarn());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test: {@link OrderMapper#updateOrder(Order, Order)}
     */
    @Test
    void testUpdateOrder7() {
        // Arrange
        Order dbOrder = new Order();
        dbOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        dbOrder.setCustomerId(UUID.randomUUID());
        dbOrder.setDeliveryAddress(UUID.randomUUID());
        dbOrder.setId(UUID.randomUUID());
        dbOrder.setProducts(new ArrayList<>());
        dbOrder.setStatus(OrderStatus.INIT);
        dbOrder.setTotalPrice(10.0d);
        dbOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        dbOrder.setUpdatedBy("2020-03-01");
        dbOrder.setWarn("Warn");

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("null");

        // Act
        OrderMapper.updateOrder(dbOrder, entity);

        // Assert
        assertEquals(10.0d, dbOrder.getTotalPrice());
        assertEquals(2, dbOrder.getCustomerId().variant());
        assertEquals(2, dbOrder.getDeliveryAddress().variant());
        assertEquals(OrderStatus.INIT, dbOrder.getStatus());
    }

    /**
     * Method under test:
     * {@link OrderMapper#updateOrderProductsFromStockEvent(Order, StockResponse)}
     */
    @Test
    void testUpdateOrderProductsFromStockEvent() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        UUID id = UUID.randomUUID();

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(id, new ArrayList<>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    /**
     * Method under test:
     * {@link OrderMapper#updateOrderProductsFromStockEvent(Order, StockResponse)}
     */
    @Test
    void testUpdateOrderProductsFromStockEvent2() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Total price of the order : {}");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        UUID id = UUID.randomUUID();

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(id, new ArrayList<>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    /**
     * Method under test:
     * {@link OrderMapper#updateOrderProductsFromStockEvent(Order, StockResponse)}
     */
    @Test
    void testUpdateOrderProductsFromStockEvent3() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Total price of the order : {}");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setId(UUID.randomUUID());
        orderItem2.setName("Total price of the order : {}");
        orderItem2.setOrderItemId(2L);
        orderItem2.setPrice(0.0d);
        orderItem2.setQuantity(1);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem2);
        orderItems.add(orderItem);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        UUID id = UUID.randomUUID();

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(id, new ArrayList<>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    /**
     * Method under test:
     * {@link OrderMapper#updateOrderProductsFromStockEvent(Order, StockResponse)}
     */
    @Test
    void testUpdateOrderProductsFromStockEvent4() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Total price of the order : {}");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setId(UUID.randomUUID());
        orderItem2.setName("Total price of the order : {}");
        orderItem2.setOrderItemId(2L);
        orderItem2.setPrice(0.0d);
        orderItem2.setQuantity(1);

        OrderItem orderItem3 = new OrderItem();
        orderItem3.setId(UUID.randomUUID());
        orderItem3.setName(", ");
        orderItem3.setOrderItemId(3L);
        orderItem3.setPrice(0.5d);
        orderItem3.setQuantity(2);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem3);
        orderItems.add(orderItem2);
        orderItems.add(orderItem);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        UUID id = UUID.randomUUID();

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(id, new ArrayList<>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    /**
     * Method under test: {@link OrderMapper#updateCustomer(Order, Customer)}
     */
    @Test
    void testUpdateCustomer() {
        // Arrange
        Order newOrder = new Order();
        newOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        newOrder.setCustomerId(UUID.randomUUID());
        newOrder.setDeliveryAddress(UUID.randomUUID());
        newOrder.setId(UUID.randomUUID());
        newOrder.setProducts(new ArrayList<>());
        newOrder.setStatus(OrderStatus.INIT);
        newOrder.setTotalPrice(10.0d);
        newOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setUpdatedBy("2020-03-01");
        newOrder.setWarn("Warn");

        Address address = new Address();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setStreet("Updating customer for order {}");
        address.setZipcode("21654");

        Customer customer = new Customer();
        customer.setAddress(address);

        // Act
        OrderMapper.updateCustomer(newOrder, customer);

        // Assert
        assertNull(newOrder.getCustomerId());
        assertEquals(2, newOrder.getDeliveryAddress().variant());
    }

    @Test
    void testUpdateOrderProducts() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(order.getId(), new ArrayList<Product>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    @Test
    void testUpdateOrderProducts3() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Total price of the order : {}");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);

        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(orderItems);
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");

        StockResponse products = new StockResponse(order.getId(), new ArrayList<>());

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, products);

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }


    @Test
    void testUpdateOrderProducts2() {
        // Arrange
        Order order = newTestOrder();

        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, new StockResponse(order.getId(), new ArrayList<>()));

        // Assert
        assertEquals(0.0d, order.getTotalPrice());
    }

    @Test
    void testUpdateOrderProductsWithoutProductsMatching() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setQuantity(3);
        Order order = newTestOrder();
        order.setCustomerId(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(List.of(orderItem));
        StockResponse responseEvent = new StockResponse(order.getId(), List.of(new Product(UUID.randomUUID())));
        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, responseEvent);
        //  Assert
        OrderItem notInStock = order.getProducts().getFirst();
        assertEquals(3, notInStock.getQuantity()); // requested amount did not change
        assertNull(notInStock.getName());
        assertEquals(0.0d, order.getTotalPrice());
        assertNull(notInStock.getPrice());
        assertThrows(OrderValidationError.class, order::validate);
    }

    @Test
    void testUpdateOrderProductsWithNotEnoughItemsInStock() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setQuantity(3);
        Order order = newTestOrder();
        order.getProducts().add(orderItem);
        Product product = new Product(orderItem.getId());
        product.setStock(2);
        product.getDetails().setPrice(5.0d);
        StockResponse responseEvent = new StockResponse(order.getId(), List.of(product));
        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, responseEvent);
        //  Assert
        OrderItem notInStock = order.getProducts().getFirst();
        assertEquals(2, orderItem.getQuantity()); // requested amount did change due to stock amount
        assertNull(notInStock.getName());
        assertEquals(product.getStock() * product.getDetails().getPrice(), order.getTotalPrice()); // total price is the price of the available items
        assertTrue(order.getWarn().contains("3 requested but only 2 available"));
    }

    @Test
    void testUpdateOrderProductsWithEnoughItemsInStock() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setQuantity(10);
        Order order = newTestOrder();
        order.getProducts().add(orderItem);
        Product product = new Product(orderItem.getId());
        product.setStock(12);
        product.getDetails().setPrice(5.0d);
        StockResponse responseEvent = new StockResponse(order.getId(), List.of(product));
        // Act
        OrderMapper.updateOrderProductsFromStockEvent(order, responseEvent);
        //  Assert
        OrderItem notInStock = order.getProducts().getFirst();
        assertEquals(10, orderItem.getQuantity()); // requested amount did change due to stock amount
        assertNull(notInStock.getName());
        assertEquals(50, order.getTotalPrice()); // total price is the price of the available items
    }

    @Test
    void testUpdateOrderWithBadStatus() {
        // Arrange
        Order order = newTestOrder();
        order.setStatus(null);
        // Act & Assert
        assertThrows(OrderValidationError.class, order::validate);
    }

    /**
     * Method under test: {@link OrderMapper#updateCustomer(Order, Customer)}
     */
    @Test
    void testUpdateCustomer1() {
        // Arrange
        Order newOrder = mock(Order.class);
        doNothing().when(newOrder).setCustomerId(Mockito.any());
        doNothing().when(newOrder).setDeliveryAddress(Mockito.any());
        doNothing().when(newOrder).setProducts(Mockito.any());
        doNothing().when(newOrder).setStatus(Mockito.any());
        doNothing().when(newOrder).setTotalPrice(anyDouble());
        doNothing().when(newOrder).setId(Mockito.any());
        doNothing().when(newOrder).setCreatedAt(Mockito.any());
        doNothing().when(newOrder).setCreatedBy(Mockito.any());
        doNothing().when(newOrder).setUpdatedAt(Mockito.any());
        doNothing().when(newOrder).setUpdatedBy(Mockito.any());
        newOrder.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        newOrder.setCustomerId(UUID.randomUUID());
        newOrder.setDeliveryAddress(UUID.randomUUID());
        newOrder.setId(UUID.randomUUID());
        newOrder.setProducts(new ArrayList<>());
        newOrder.setStatus(OrderStatus.INIT);
        newOrder.setTotalPrice(10.0d);
        newOrder.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        newOrder.setUpdatedBy("2020-03-01");

        Address address = new Address();
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setStreet("Street");
        address.setZipcode("21654");

        Customer payload = new Customer();
        payload.setAddress(address);

        // Act
        OrderMapper.updateCustomer(newOrder, payload);

        // Assert that nothing has changed
        verify(newOrder).setCustomerId(isA(UUID.class));
        verify(newOrder, atLeast(1)).setDeliveryAddress(Mockito.any());
        verify(newOrder).setProducts(isA(List.class));
        verify(newOrder).setStatus(eq(OrderStatus.INIT));
        verify(newOrder).setTotalPrice(eq(10.0d));
        verify(newOrder).setId(isA(UUID.class));
        verify(newOrder).setCreatedAt(isA(LocalDateTime.class));
        verify(newOrder).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(newOrder).setUpdatedAt(isA(LocalDateTime.class));
        verify(newOrder).setUpdatedBy(eq("2020-03-01"));
        assertEquals("Oxford", payload.getAddress().getCity());
    }
}
