package fr.epsi.rennes.mspr.tpre814.orders.database;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderHistory;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ContextConfiguration(classes = {OrderHistory.class})
@ExtendWith(SpringExtension.class)
class OrderHistoryDiffblueTest {
    @Autowired
    private OrderHistory orderHistory;

    /**
     * Method under test: {@link OrderHistory#OrderHistory()}
     */
    @Test
    void testNewOrderHistory() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory();

        // Assert
        assertNull(actualOrderHistory.getOrder());
        assertNull(actualOrderHistory.getStatus());
        assertNull(actualOrderHistory.getId());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory2() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.INIT);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.INIT, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory3() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.RESERVED);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.RESERVED, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory4() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.PARTIAL);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.PARTIAL, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory5() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.CONFIRMED);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.CONFIRMED, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory6() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.READY);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.READY, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory7() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.CANCELLED);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.CANCELLED, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory8() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.CLOSED);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.CLOSED, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory9() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.ARCHIVED);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.ARCHIVED, actualOrderHistory.getStatus());
    }

    /**
     * Method under test: {@link OrderHistory#OrderHistory(UUID, OrderStatus)}
     */
    @Test
    void testNewOrderHistory10() {
        // Arrange and Act
        OrderHistory actualOrderHistory = new OrderHistory(UUID.randomUUID(), OrderStatus.UNKNOWN);

        // Assert
        Order order = actualOrderHistory.getOrder();
        assertNull(order.getCreatedAt());
        assertEquals(2, order.getId().variant());
        assertEquals(OrderStatus.UNKNOWN, actualOrderHistory.getStatus());
    }
}
