package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderHistory;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderHistoryRepository;
import fr.epsi.rennes.mspr.tpre814.orders.database.repository.OrderRepository;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderStatusException;
import fr.epsi.rennes.mspr.tpre814.orders.errors.OrderValidationError;
import fr.epsi.rennes.mspr.tpre814.orders.services.interfaces.OrderService;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.events.BaseEvent;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import org.assertj.core.api.junit.jupiter.SoftAssertionsExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.test.context.aot.DisabledInAotMode;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static fr.epsi.rennes.mspr.tpre814.orders.Stubs.newTestOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

//@SpringBootTest
@DisabledInAotMode
//@ExtendWith(SpringExtension.class)
@ExtendWith(SoftAssertionsExtension.class)
class OrderServiceImplTestFunctional {
    private KafkaService kafka;
    private OrderHistoryRepository orderHistoryRepository;
    private OrderRepository orderRepository;

    private OrderServiceImpl orderServiceImpl;


    @BeforeEach
    void setUp() {
        kafka = mock(KafkaService.class);
        orderRepository = mock(OrderRepository.class);
        orderHistoryRepository = mock(OrderHistoryRepository.class);

        orderServiceImpl = new OrderServiceImpl(kafka, orderRepository, orderHistoryRepository);
    }

    /**
     * Method under test: {@link OrderServiceImpl#setUp()}
     */
    @Test
    void testSetUp() {
        // Arrange
        // Act
        // Assert that log4j logged in info "Order Service started"
        assertDoesNotThrow(() -> orderServiceImpl.setUp());
    }

    /**
     * Method under test: {@link OrderServiceImpl#get(UUID)}
     */
    @Test
    void testGet() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);

        // Act
        Order actualGetResult = orderServiceImpl.get(UUID.randomUUID());

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        assertSame(order, actualGetResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#get(UUID)}
     */
    @Test
    void testGet2() {
        // Arrange
        Optional<Order> emptyResult = Optional.empty();
        when(orderRepository.findById(Mockito.any())).thenReturn(emptyResult);

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.get(UUID.randomUUID()));
        verify(orderRepository).findById(isA(UUID.class));
    }

    @Test
    void testOrderIdMustBeNull() {
        Order order = new Order();
        order.setId(UUID.randomUUID());
        assertThrows(OrderValidationError.class, () -> orderServiceImpl.create(order));
    }

    /**
     * Method under test: {@link OrderServiceImpl#all()}
     */
    @Test
    void testAll() {
        // Arrange
        ArrayList<Order> orderList = new ArrayList<>();
        when(orderRepository.findAll()).thenReturn(orderList);

        // Act
        List<Order> actualAllResult = orderServiceImpl.all();

        // Assert
        verify(orderRepository).findAll();
        assertTrue(actualAllResult.isEmpty());
        assertSame(orderList, actualAllResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        Order actualUpdateResult = orderServiceImpl.update(entity);

        // Assert
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertSame(order2, actualUpdateResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        orderServiceImpl.update(entity);

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate3() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getStatus()).thenReturn(OrderStatus.INIT);
        when(order.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        Order entity = new Order();
        entity.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        entity.setCustomerId(UUID.randomUUID());
        entity.setDeliveryAddress(UUID.randomUUID());
        entity.setId(UUID.randomUUID());
        entity.setProducts(new ArrayList<>());
        entity.setStatus(OrderStatus.INIT);
        entity.setTotalPrice(10.0d);
        entity.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        entity.setUpdatedBy("2020-03-01");
        entity.setWarn("Warn");

        // Act
        orderServiceImpl.update(entity);

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order, atLeast(1)).setUpdatedAt(Mockito.any());
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order, atLeast(1)).setCustomerId(Mockito.any());
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order, atLeast(1)).setDeliveryAddress(Mockito.any());
        verify(order2).setProducts(isA(List.class));
        verify(order).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order, atLeast(1)).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order, atLeast(1)).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order, atLeast(1)).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#update(Order)}
     */
    @Test
    void testUpdate4() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getStatus()).thenReturn(OrderStatus.RESERVED);
        when(order.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        Order entity = newTestOrder();
        entity.setWarn("Warn");

        // Act
        orderServiceImpl.update(entity);

        // Assert
        verify(order, atLeast(1)).getId();
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order, atLeast(1)).setUpdatedAt(Mockito.any());
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order, atLeast(1)).getStatus();
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order, atLeast(1)).setCustomerId(Mockito.any());
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order, atLeast(1)).setDeliveryAddress(Mockito.any());
        verify(order2).setProducts(isA(List.class));
        verify(order).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order, atLeast(1)).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order, atLeast(1)).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order, atLeast(1)).setWarn(eq("Warn"));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository, atLeast(1)).save(isA(Order.class));
        verify(orderHistoryRepository, atLeast(1)).save(Mockito.any());
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#delete(UUID)}
     */
    @Test
    void testDelete3() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getStatus()).thenReturn(OrderStatus.INIT);
        when(order.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.delete(UUID.randomUUID());

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order).setProducts(isA(List.class));
        verify(order, atLeast(1)).setStatus(Mockito.any());
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order).setWarn(eq("Warn"));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.cancel(UUID.randomUUID());

        // Assert
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel2() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.cancel(UUID.randomUUID());

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#cancel(UUID)}
     */
    @Test
    void testCancel3() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getStatus()).thenReturn(OrderStatus.INIT);
        when(order.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());

        // Act
        orderServiceImpl.cancel(UUID.randomUUID());

        // Assert
        verify(order2, atLeast(1)).getId();
        verify(order, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order2, atLeast(1)).getStatus();
        verify(order, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order).setProducts(isA(List.class));
        verify(order, atLeast(1)).setStatus(Mockito.any());
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order).setWarn(eq("Warn"));
        verify(orderRepository, atLeast(1)).findById(Mockito.any());
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.close(order));
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose2() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getStatus()).thenReturn(OrderStatus.INIT);
        when(order.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.close(order));
        verify(order).getId();
        verify(order).setId(isA(UUID.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setUpdatedAt(isA(LocalDateTime.class));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order, atLeast(1)).getStatus();
        verify(order).setCustomerId(isA(UUID.class));
        verify(order).setDeliveryAddress(isA(UUID.class));
        verify(order).setProducts(isA(List.class));
        verify(order).setStatus(eq(OrderStatus.INIT));
        verify(order).setTotalPrice(eq(10.0d));
        verify(order).setWarn(eq("Warn"));
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose3() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        Order order3 = mock(Order.class);
        when(order3.getStatus()).thenReturn(OrderStatus.CONFIRMED);
        when(order3.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order3).setId(Mockito.any());
        doNothing().when(order3).setCreatedAt(Mockito.any());
        doNothing().when(order3).setCreatedBy(Mockito.any());
        doNothing().when(order3).setUpdatedAt(Mockito.any());
        doNothing().when(order3).setUpdatedBy(Mockito.any());
        doNothing().when(order3).setCustomerId(Mockito.any());
        doNothing().when(order3).setDeliveryAddress(Mockito.any());
        doNothing().when(order3).setProducts(Mockito.any());
        doNothing().when(order3).setStatus(Mockito.any());
        doNothing().when(order3).setTotalPrice(anyDouble());
        doNothing().when(order3).setWarn(Mockito.any());
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.INIT);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualCloseResult = orderServiceImpl.close(order3);

        // Assert
        verify(order3).getId();
        verify(order3).setId(isA(UUID.class));
        verify(order3).setCreatedAt(isA(LocalDateTime.class));
        verify(order3).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order3).setUpdatedAt(isA(LocalDateTime.class));
        verify(order3).setUpdatedBy(eq("2020-03-01"));
        verify(order3).getStatus();
        verify(order3).setCustomerId(isA(UUID.class));
        verify(order3).setDeliveryAddress(isA(UUID.class));
        verify(order3).setProducts(isA(List.class));
        verify(order3).setStatus(eq(OrderStatus.INIT));
        verify(order3).setTotalPrice(eq(10.0d));
        verify(order3).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertTrue(actualCloseResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose4() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        Order order3 = mock(Order.class);
        when(order3.getStatus()).thenReturn(OrderStatus.CONFIRMED);
        when(order3.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order3).setId(Mockito.any());
        doNothing().when(order3).setCreatedAt(Mockito.any());
        doNothing().when(order3).setCreatedBy(Mockito.any());
        doNothing().when(order3).setUpdatedAt(Mockito.any());
        doNothing().when(order3).setUpdatedBy(Mockito.any());
        doNothing().when(order3).setCustomerId(Mockito.any());
        doNothing().when(order3).setDeliveryAddress(Mockito.any());
        doNothing().when(order3).setProducts(Mockito.any());
        doNothing().when(order3).setStatus(Mockito.any());
        doNothing().when(order3).setTotalPrice(anyDouble());
        doNothing().when(order3).setWarn(Mockito.any());
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.INIT);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualCloseResult = orderServiceImpl.close(order3);

        // Assert
        verify(order3).getId();
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order3).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order3).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order3).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order3).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order3).setUpdatedBy(eq("2020-03-01"));
        verify(order3).getStatus();
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order3).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order3).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order3).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order3).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order3).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order3).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertTrue(actualCloseResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#close(Order)}
     */
    @Test
    void testClose5() {
        // Arrange
        Order order = mock(Order.class);
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        Order order3 = mock(Order.class);
        when(order3.getStatus()).thenReturn(OrderStatus.CONFIRMED);
        when(order3.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order3).setId(Mockito.any());
        doNothing().when(order3).setCreatedAt(Mockito.any());
        doNothing().when(order3).setCreatedBy(Mockito.any());
        doNothing().when(order3).setUpdatedAt(Mockito.any());
        doNothing().when(order3).setUpdatedBy(Mockito.any());
        doNothing().when(order3).setCustomerId(Mockito.any());
        doNothing().when(order3).setDeliveryAddress(Mockito.any());
        doNothing().when(order3).setProducts(Mockito.any());
        doNothing().when(order3).setStatus(Mockito.any());
        doNothing().when(order3).setTotalPrice(anyDouble());
        doNothing().when(order3).setWarn(Mockito.any());
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.INIT);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualCloseResult = orderServiceImpl.close(order3);

        // Assert
        verify(order3).getId();
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order).setId(isA(UUID.class));
        verify(order3).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order3).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order3).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order).setUpdatedAt(isA(LocalDateTime.class));
        verify(order3).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order3).setUpdatedBy(eq("2020-03-01"));
        verify(order3).getStatus();
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order).setCustomerId(isA(UUID.class));
        verify(order3).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order).setDeliveryAddress(isA(UUID.class));
        verify(order3).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order).setProducts(isA(List.class));
        verify(order3).setProducts(isA(List.class));
        verify(order, atLeast(1)).setStatus(Mockito.any());
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order3).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order).setTotalPrice(eq(10.0d));
        verify(order3).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order).setWarn(eq("Warn"));
        verify(order3).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertTrue(actualCloseResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.archive(order));
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive2() {
        // Arrange
        Order order = mock(Order.class);
        when(order.getId()).thenReturn(UUID.randomUUID());
        when(order.getStatus()).thenReturn(OrderStatus.INIT);
        doNothing().when(order).setId(Mockito.any());
        doNothing().when(order).setCreatedAt(Mockito.any());
        doNothing().when(order).setCreatedBy(Mockito.any());
        doNothing().when(order).setUpdatedAt(Mockito.any());
        doNothing().when(order).setUpdatedBy(Mockito.any());
        doNothing().when(order).setCustomerId(Mockito.any());
        doNothing().when(order).setDeliveryAddress(Mockito.any());
        doNothing().when(order).setProducts(Mockito.any());
        doNothing().when(order).setStatus(Mockito.any());
        doNothing().when(order).setTotalPrice(anyDouble());
        doNothing().when(order).setWarn(Mockito.any());
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");

        // Act and Assert
        assertThrows(OrderStatusException.class, () -> orderServiceImpl.archive(order));
        verify(order).getId();
        verify(order).setId(isA(UUID.class));
        verify(order).setCreatedAt(isA(LocalDateTime.class));
        verify(order).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order).setUpdatedAt(isA(LocalDateTime.class));
        verify(order).setUpdatedBy(eq("2020-03-01"));
        verify(order, atLeast(1)).getStatus();
        verify(order).setCustomerId(isA(UUID.class));
        verify(order).setDeliveryAddress(isA(UUID.class));
        verify(order).setProducts(isA(List.class));
        verify(order).setStatus(eq(OrderStatus.INIT));
        verify(order).setTotalPrice(eq(10.0d));
        verify(order).setWarn(eq("Warn"));
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive3() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);

        Order order2 = new Order();
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        Order order3 = mock(Order.class);
        when(order3.getId()).thenReturn(UUID.randomUUID());
        when(order3.getStatus()).thenReturn(OrderStatus.CLOSED);
        doNothing().when(order3).setId(Mockito.any());
        doNothing().when(order3).setCreatedAt(Mockito.any());
        doNothing().when(order3).setCreatedBy(Mockito.any());
        doNothing().when(order3).setUpdatedAt(Mockito.any());
        doNothing().when(order3).setUpdatedBy(Mockito.any());
        doNothing().when(order3).setCustomerId(Mockito.any());
        doNothing().when(order3).setDeliveryAddress(Mockito.any());
        doNothing().when(order3).setProducts(Mockito.any());
        doNothing().when(order3).setStatus(Mockito.any());
        doNothing().when(order3).setTotalPrice(anyDouble());
        doNothing().when(order3).setWarn(Mockito.any());
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.INIT);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualArchiveResult = orderServiceImpl.archive(order3);

        // Assert
        verify(order3).getId();
        verify(order3).setId(isA(UUID.class));
        verify(order3).setCreatedAt(isA(LocalDateTime.class));
        verify(order3).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order3).setUpdatedAt(isA(LocalDateTime.class));
        verify(order3).setUpdatedBy(eq("2020-03-01"));
        verify(order3).getStatus();
        verify(order3).setCustomerId(isA(UUID.class));
        verify(order3).setDeliveryAddress(isA(UUID.class));
        verify(order3).setProducts(isA(List.class));
        verify(order3).setStatus(eq(OrderStatus.INIT));
        verify(order3).setTotalPrice(eq(10.0d));
        verify(order3).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertTrue(actualArchiveResult);
    }

    /**
     * Method under test: {@link OrderServiceImpl#archive(Order)}
     */
    @Test
    void testArchive4() {
        // Arrange
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        order.setWarn("Warn");
        Optional<Order> ofResult = Optional.of(order);
        Order order2 = mock(Order.class);
        when(order2.getStatus()).thenReturn(OrderStatus.INIT);
        when(order2.getId()).thenReturn(UUID.randomUUID());
        doNothing().when(order2).setId(Mockito.any());
        doNothing().when(order2).setCreatedAt(Mockito.any());
        doNothing().when(order2).setCreatedBy(Mockito.any());
        doNothing().when(order2).setUpdatedAt(Mockito.any());
        doNothing().when(order2).setUpdatedBy(Mockito.any());
        doNothing().when(order2).setCustomerId(Mockito.any());
        doNothing().when(order2).setDeliveryAddress(Mockito.any());
        doNothing().when(order2).setProducts(Mockito.any());
        doNothing().when(order2).setStatus(Mockito.any());
        doNothing().when(order2).setTotalPrice(anyDouble());
        doNothing().when(order2).setWarn(Mockito.any());
        order2.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order2.setCustomerId(UUID.randomUUID());
        order2.setDeliveryAddress(UUID.randomUUID());
        order2.setId(UUID.randomUUID());
        order2.setProducts(new ArrayList<>());
        order2.setStatus(OrderStatus.INIT);
        order2.setTotalPrice(10.0d);
        order2.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order2.setUpdatedBy("2020-03-01");
        order2.setWarn("Warn");
        when(orderRepository.save(Mockito.any())).thenReturn(order2);
        when(orderRepository.findById(Mockito.any())).thenReturn(ofResult);
        when(orderHistoryRepository.save(Mockito.any())).thenReturn(new OrderHistory());
        Order order3 = mock(Order.class);
        when(order3.getId()).thenReturn(UUID.randomUUID());
        when(order3.getStatus()).thenReturn(OrderStatus.CLOSED);
        doNothing().when(order3).setId(Mockito.any());
        doNothing().when(order3).setCreatedAt(Mockito.any());
        doNothing().when(order3).setCreatedBy(Mockito.any());
        doNothing().when(order3).setUpdatedAt(Mockito.any());
        doNothing().when(order3).setUpdatedBy(Mockito.any());
        doNothing().when(order3).setCustomerId(Mockito.any());
        doNothing().when(order3).setDeliveryAddress(Mockito.any());
        doNothing().when(order3).setProducts(Mockito.any());
        doNothing().when(order3).setStatus(Mockito.any());
        doNothing().when(order3).setTotalPrice(anyDouble());
        doNothing().when(order3).setWarn(Mockito.any());
        order3.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order3.setCustomerId(UUID.randomUUID());
        order3.setDeliveryAddress(UUID.randomUUID());
        order3.setId(UUID.randomUUID());
        order3.setProducts(new ArrayList<>());
        order3.setStatus(OrderStatus.INIT);
        order3.setTotalPrice(10.0d);
        order3.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order3.setUpdatedBy("2020-03-01");
        order3.setWarn("Warn");

        // Act
        boolean actualArchiveResult = orderServiceImpl.archive(order3);

        // Assert
        verify(order3).getId();
        verify(order2, atLeast(1)).getId();
        verify(order2).setId(isA(UUID.class));
        verify(order3).setId(isA(UUID.class));
        verify(order2).setCreatedAt(isA(LocalDateTime.class));
        verify(order3).setCreatedAt(isA(LocalDateTime.class));
        verify(order2).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order3).setCreatedBy(eq("Jan 1, 2020 8:00am GMT+0100"));
        verify(order2).setUpdatedAt(isA(LocalDateTime.class));
        verify(order3).setUpdatedAt(isA(LocalDateTime.class));
        verify(order2).setUpdatedBy(eq("2020-03-01"));
        verify(order3).setUpdatedBy(eq("2020-03-01"));
        verify(order3).getStatus();
        verify(order2, atLeast(1)).getStatus();
        verify(order2).setCustomerId(isA(UUID.class));
        verify(order3).setCustomerId(isA(UUID.class));
        verify(order2).setDeliveryAddress(isA(UUID.class));
        verify(order3).setDeliveryAddress(isA(UUID.class));
        verify(order2).setProducts(isA(List.class));
        verify(order3).setProducts(isA(List.class));
        verify(order2).setStatus(eq(OrderStatus.INIT));
        verify(order3).setStatus(eq(OrderStatus.INIT));
        verify(order2).setTotalPrice(eq(10.0d));
        verify(order3).setTotalPrice(eq(10.0d));
        verify(order2).setWarn(eq("Warn"));
        verify(order3).setWarn(eq("Warn"));
        verify(orderRepository).findById(isA(UUID.class));
        verify(orderRepository).save(isA(Order.class));
        verify(orderHistoryRepository).save(isA(OrderHistory.class));
        assertTrue(actualArchiveResult);
    }

    /**
     *
     */
    @Test
    void testValidate() throws JsonProcessingException {
        // Arrange
        Customer event = new Customer();
        event.setId(UUID.randomUUID());
        // Act
        kafka.listenCustomer(event.toJson(), event.getId());

        // Assert that nothing has changed
        assertNotNull(event.getAddress());
        assertNotNull(event.getCompany());
        assertNotNull(event.getProfile());
        assertNull(event.getUsername());
        assertNotNull(event.getId());
    }

    /**
     *
     */
    @Test
    void testValidate2() throws JsonProcessingException {
        // Arrange
        Address address = mock(Address.class);
        doNothing().when(address).setCity(Mockito.any());
        doNothing().when(address).setCountry(Mockito.any());
        doNothing().when(address).setId(Mockito.any());
        doNothing().when(address).setStreet(Mockito.any());
        doNothing().when(address).setZipcode(Mockito.any());
        address.setCity("Oxford");
        address.setCountry("GB");
        address.setId(UUID.randomUUID());
        address.setStreet("AnyStreet");
        address.setZipcode("21654");

        Customer event = new Customer();
        event.setId(UUID.randomUUID());
        event.setAddress(address);
        CompletableFuture<BaseEvent> future = new CompletableFuture<>();
        future.complete(event);
        // Act
        kafka.listenCustomer(event.toJson(), event.getId());

        // Assert that nothing has changed
        verify(address).setCity(eq("Oxford"));
        verify(address).setCountry(eq("GB"));
        verify(address).setId(isA(UUID.class));
        verify(address).setStreet(eq("AnyStreet"));
        verify(address).setZipcode(eq("21654"));
    }

    @Test
    public void testChangeStatus_Success() {
        OrderService orderService = new OrderServiceImpl(mock(KafkaService.class), orderRepository, orderHistoryRepository);
        UUID orderId = UUID.randomUUID();
        Order order = new Order();
        order.setId(orderId);
        order.setStatus(OrderStatus.INIT);

        when(orderRepository.findById(orderId)).thenReturn(Optional.of(order));
        when(orderRepository.save(any(Order.class))).thenReturn(order);
        // deep copy
        Order order2 = new Order();
        order2.setId(order.getId());
        order2.setCreatedAt(order.getCreatedAt());
        order2.setCreatedBy(order.getCreatedBy());
        order2.setUpdatedAt(order.getUpdatedAt());
        order2.setUpdatedBy(order.getUpdatedBy());
        order2.setCustomerId(order.getCustomerId());
        order2.setDeliveryAddress(order.getDeliveryAddress());
        order2.setProducts(order.getProducts());
        order2.setTotalPrice(order.getTotalPrice());
        order2.setWarn(order.getWarn());
        // Alter status manually
        order.setStatus(OrderStatus.CONFIRMED);
        orderService.update(order);

        verify(orderRepository, times(1)).findById(orderId);
        verify(orderRepository, times(1)).save(order);
        assert order.getStatus() == OrderStatus.CONFIRMED;
    }
}
