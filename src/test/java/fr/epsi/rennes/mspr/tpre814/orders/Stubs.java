package fr.epsi.rennes.mspr.tpre814.orders;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import org.jetbrains.annotations.NotNull;
import org.testcontainers.shaded.org.checkerframework.checker.nullness.qual.NonNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

public class Stubs {
    @NotNull
    public static Order newTestOrder() {
        Order order = new Order();
        order.setCreatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setCreatedBy("Jan 1, 2020 8:00am GMT+0100");
        order.setCustomerId(UUID.randomUUID());
        order.setDeliveryAddress(UUID.randomUUID());
        order.setId(UUID.randomUUID());
        order.setProducts(new ArrayList<>());
        order.setStatus(OrderStatus.INIT);
        order.setTotalPrice(10.0d);
        order.setUpdatedAt(LocalDate.of(1970, 1, 1).atStartOfDay());
        order.setUpdatedBy("2020-03-01");
        return order;
    }

    @NonNull
    public static Product newTestProduct() {
        Product product = new Product();
        product.setId(UUID.randomUUID());
        product.setName("product");
        product.setQuantity(1);
        product.setStock(10);
        ProductDetails productDetails = new ProductDetails();
        productDetails.setPrice(100.0);
        productDetails.setName("product");
        productDetails.setDescription("some item!");
        product.setDetails(productDetails);
        return product;
    }
}
