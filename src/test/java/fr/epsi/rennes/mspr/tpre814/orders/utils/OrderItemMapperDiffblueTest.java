package fr.epsi.rennes.mspr.tpre814.orders.utils;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.shared.events.ProductRequest;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;
import fr.epsi.rennes.mspr.tpre814.shared.utils.ItemMapper;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class OrderItemMapperDiffblueTest {
    /**
     * Method under test: {@link OrderItemMapper#toProducts(List)}
     */
    @Test
    void testToProducts() {
        // Arrange and Act
        List<Product> actualToProductsResult = OrderItemMapper.toProducts(new ArrayList<>());

        // Assert
        assertTrue(actualToProductsResult.isEmpty());
    }

    /**
     * Method under test: {@link OrderItemMapper#toProducts(List)}
     */
    @Test
    void testToProducts2() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Name");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> source = new ArrayList<>();
        source.add(orderItem);

        // Act
        List<Product> actualToProductsResult = OrderItemMapper.toProducts(source);

        // Assert
        assertEquals(1, actualToProductsResult.size());
        Product getResult = actualToProductsResult.get(0);
        assertEquals("Name", getResult.getName());
        ProductDetails details = getResult.getDetails();
        assertNull(details.getDescription());
        assertEquals(1, getResult.getQuantity());
        assertEquals(10.0d, details.getPrice());
        assertEquals(2, getResult.getId().variant());
    }

    /**
     * Method under test: {@link OrderItemMapper#toProducts(List)}
     */
    @Test
    void testToProducts3() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Name");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        OrderItem orderItem2 = new OrderItem();
        orderItem2.setId(UUID.randomUUID());
        orderItem2.setName("42");
        orderItem2.setOrderItemId(2L);
        orderItem2.setPrice(0.5d);
        orderItem2.setQuantity(0);

        ArrayList<OrderItem> source = new ArrayList<>();
        source.add(orderItem2);
        source.add(orderItem);

        // Act
        List<Product> actualToProductsResult = OrderItemMapper.toProducts(source);

        // Assert
        assertEquals(2, actualToProductsResult.size());
        Product getResult = actualToProductsResult.get(0);
        assertEquals("42", getResult.getName());
        Product getResult2 = actualToProductsResult.get(1);
        assertEquals("Name", getResult2.getName());
        ProductDetails details = getResult.getDetails();
        assertNull(details.getDescription());
        ProductDetails details2 = getResult2.getDetails();
        assertNull(details2.getDescription());
        assertEquals(0, getResult.getQuantity());
        assertEquals(0.5d, details.getPrice());
        assertEquals(1, getResult2.getQuantity());
        assertEquals(10.0d, details2.getPrice());
        assertEquals(2, getResult.getId().variant());
        assertEquals(2, getResult2.getId().variant());
    }

    /**
     * Method under test: {@link OrderItemMapper#toProducts(List)}
     */
    @Test
    void testToProducts4() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName(null);
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(10.0d);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> source = new ArrayList<>();
        source.add(orderItem);

        // Act
        List<Product> actualToProductsResult = OrderItemMapper.toProducts(source);

        // Assert
        assertEquals(1, actualToProductsResult.size());
        Product getResult = actualToProductsResult.get(0);
        ProductDetails details = getResult.getDetails();
        assertNull(details.getDescription());
        assertEquals(1, getResult.getQuantity());
        assertEquals(10.0d, details.getPrice());
        assertEquals(2, getResult.getId().variant());
    }

    /**
     * Method under test: {@link OrderItemMapper#toProducts(List)}
     */
    @Test
    void testToProducts5() {
        // Arrange
        OrderItem orderItem = new OrderItem();
        orderItem.setId(UUID.randomUUID());
        orderItem.setName("Name");
        orderItem.setOrderItemId(1L);
        orderItem.setPrice(null);
        orderItem.setQuantity(1);

        ArrayList<OrderItem> source = new ArrayList<>();
        source.add(orderItem);

        // Act
        List<Product> actualToProductsResult = OrderItemMapper.toProducts(source);

        // Assert
        assertEquals(1, actualToProductsResult.size());
        Product getResult = actualToProductsResult.get(0);
        assertEquals("Name", getResult.getName());
        assertNull(getResult.getDetails().getDescription());
        assertEquals(1, getResult.getQuantity());
        assertEquals(2, getResult.getId().variant());
    }

    /**
     * Method under test: {@link OrderItemMapper#toOrderItems(List)}
     */
    @Test
    void testToOrderItems() {
        // Arrange and Act
        List<OrderItem> actualToOrderItemsResult = OrderItemMapper.toOrderItems(new ArrayList<>());

        // Assert
        assertTrue(actualToOrderItemsResult.isEmpty());
    }

    /**
     * Method under test: {@link OrderItemMapper#toOrderItems(List)}
     */
    @Test
    void testToOrderItems2() {
        // Arrange
        Product product = new Product();
        product.setDetails(new ProductDetails());

        ArrayList<Product> source = new ArrayList<>();
        source.add(product);

        // Act
        List<OrderItem> actualToOrderItemsResult = OrderItemMapper.toOrderItems(source);

        // Assert
        assertEquals(1, actualToOrderItemsResult.size());
        OrderItem getResult = actualToOrderItemsResult.get(0);
        assertNull(getResult.getName());
        assertNull(getResult.getId());
        assertEquals(0, getResult.getQuantity().intValue());
        assertEquals(0.0d, getResult.getPrice().doubleValue());
    }

    /**
     * Method under test: {@link ItemMapper#toProductRequest(List)}
     */
    @Test
    void testToProductRequest() {
        // Arrange and Act
        List<ProductRequest> actualToProductRequestResult = ItemMapper.toProductRequest(new ArrayList<>());

        // Assert
        assertTrue(actualToProductRequestResult.isEmpty());
    }

    /**
     * Method under test: {@link ItemMapper#toProductRequest(List)}
     */
    @Test
    void testToProductRequest2() {
        // Arrange
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product());

        // Act
        List<ProductRequest> actualToProductRequestResult = ItemMapper.toProductRequest(products);

        // Assert
        assertEquals(1, actualToProductRequestResult.size());
        ProductRequest getResult = actualToProductRequestResult.get(0);
        assertNull(getResult.id());
        assertEquals(0, getResult.quantity());
    }

    /**
     * Method under test: {@link ItemMapper#toProductRequest(List)}
     */
    @Test
    void testToProductRequest3() {
        // Arrange
        ArrayList<Product> products = new ArrayList<>();
        products.add(new Product());
        products.add(new Product());

        // Act
        List<ProductRequest> actualToProductRequestResult = ItemMapper.toProductRequest(products);

        // Assert
        assertEquals(2, actualToProductRequestResult.size());
        ProductRequest getResult = actualToProductRequestResult.get(0);
        assertNull(getResult.id());
        ProductRequest getResult2 = actualToProductRequestResult.get(1);
        assertNull(getResult2.id());
        assertEquals(0, getResult.quantity());
        assertEquals(0, getResult2.quantity());
    }
}
