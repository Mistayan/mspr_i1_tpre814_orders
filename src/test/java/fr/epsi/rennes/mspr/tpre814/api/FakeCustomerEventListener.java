package fr.epsi.rennes.mspr.tpre814.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerRequest;
import fr.epsi.rennes.mspr.tpre814.shared.events.CustomerResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Address;
import fr.epsi.rennes.mspr.tpre814.shared.models.Company;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.testcontainers.shaded.org.checkerframework.checker.nullness.qual.NonNull;

import java.util.UUID;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.CUSTOMER_REQUEST;
import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.CUSTOMER_RESPONSE;

/**
 * The main goal of this class is to wait for messages from the given topics to be consumed,
 * in order to execute a process and finally respond with an ACK.
 */
@Component
public class FakeCustomerEventListener { // auto plugs on tests with containers
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(FakeCustomerEventListener.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private final KafkaTemplate<UUID, String> kafkaTemplate;
    Customer customersOut;  // observable

    @Autowired
    public FakeCustomerEventListener(KafkaTemplate<UUID, String> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    @NonNull
    private static Customer fakeCustomer(@NonNull UUID customerId) {
        Customer customer = new Customer(customerId);
        customer.setUsername("Faker");
        Address address = new Address();  // Fake delivery address
        address.setId(UUID.randomUUID());
        address.setCity("Rennes");
        address.setCountry("France");
        address.setStreet("Rue de la Monnaie");
        address.setZipcode("35000");
        customer.setAddress(address);
        Company company = new Company();  // Fake company
        address.setId(UUID.randomUUID());
        company.setName("EPSI");
        company.setSiret("123456789 00001");
        customer.setCompany(company);
        return customer;
    }

    @Bean
    public NewTopic customerTopic() {
        return TopicBuilder.name(CUSTOMER_REQUEST).partitions(2).replicas(1).build();
    }

    @KafkaListener(topics = CUSTOMER_REQUEST, groupId = "fake-customer-service-1",
            autoStartup = "true", info = "Fake Customer Service Listener")
    public void emulateCustomer(String req) throws JsonProcessingException {
        log.info("Received new Customer request: {}", req);
        // Get the requested customer
        CustomerRequest request = mapper.readValue(req, CustomerRequest.class);
        customersOut = fakeCustomer(request.getId());
        // Emulate the validation of the customerRequest
        CustomerResponse response = new CustomerResponse(request.getId());
        response.setPayload(customersOut);
        kafkaTemplate.send(CUSTOMER_RESPONSE, customersOut.getId(), response.toJson());
    }

    public Customer getCustomersOut() {
        return customersOut;
    }

    public void resetCustomer() {
        customersOut = null;
    }
}
