package fr.epsi.rennes.mspr.tpre814.orders.api;

import fr.epsi.rennes.mspr.tpre814.orders.api.interfaces.CRUDInterface;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.services.interfaces.OrderService;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping(value = "/api/v2/order")
@RequiredArgsConstructor
public final class OrderController implements CRUDInterface<Order, UUID, OrderDTO> {

    private final OrderService orderService;

    @Override
    @PostMapping
    public OrderDTO create(@RequestBody @NonNull Order order) {
        return orderService.create(order);
    }


    // TO BE REMOVED (tests before broker only)
    @Override
    @GetMapping(value = {"/all"})
    public List<Order> all() {
        return orderService.all();
    }

    @Override
    @PutMapping
    public Order update(@RequestBody @NonNull Order entity) {
        return orderService.update(entity);
    }

    @Override
    @DeleteMapping(value = {"/{orderId}"})
    public void delete(@PathVariable UUID orderId) {
        orderService.delete(orderId);
    }

    @GetMapping(value = {"/{orderId}"})
    public Order get(@PathVariable UUID orderId) {
        return orderService.get(orderId);
    }
}
