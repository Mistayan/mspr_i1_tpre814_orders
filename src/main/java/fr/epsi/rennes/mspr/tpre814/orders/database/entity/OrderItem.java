package fr.epsi.rennes.mspr.tpre814.orders.database.entity;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.UUID;

@Getter
@Setter
@Entity
public class OrderItem implements Serializable {
    private static final @Serial long serialVersionUID = -7409786442773105329L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderItemId;

    private UUID id;

    private String name;

    // located in details.quantity
    private Integer quantity;
    private Integer stock;

    // located in details.price
    private Double price;

    @Override
    public String toString() {
        return String.format("{id=%s, name='%s', quantity=%s, price=%s}", id, name, quantity, price);
    }
}

