package fr.epsi.rennes.mspr.tpre814.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.TopicBuilder;


@EnableKafka
@Configuration
public class KafkaConfig {
    public static final int TX_TIMEOUT = 60;
    /**
     * Topics names definition
     */
    public static final String ORDER_TOPIC = "${spring.kafka.topic.name}";
    public static final String RESERVE_TOPIC = "stock-reserve";
    public static final String STOCK_RESPONSE = "stock-response";
    public static final String CUSTOMER_REQUEST = "customer-request";
    public static final String CUSTOMER_RESPONSE = "customer-response";
    public static final String ORDER_SERVICE = "${spring.kafka.client-id}";

    // spring beans for kafka topics creation ahead of runtime
    @Bean
    public NewTopic orderTopic() {
        return TopicBuilder.name(ORDER_TOPIC).partitions(2).replicas(1).build();
    }

}
