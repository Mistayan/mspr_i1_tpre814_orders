package fr.epsi.rennes.mspr.tpre814;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@EnableJpaAuditing
@EnableAspectJAutoProxy
//@EnableDiscoveryClient
public class OrderManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(OrderManagerApplication.class, args);
    }
}
