package fr.epsi.rennes.mspr.tpre814.orders.database.repository;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
@Transactional(value = "transactionManager")
// Watch order repository. When an order is created or updated, create a new entry in the order_history table
public interface OrderHistoryRepository extends JpaRepository<OrderHistory, UUID> {

    @Query("SELECT oh FROM OrderHistory oh WHERE oh.order = :order ORDER BY oh.at DESC")
    List<OrderHistory> findAllByOrder(Order order);
}