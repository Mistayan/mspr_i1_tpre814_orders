package fr.epsi.rennes.mspr.tpre814.orders.utils;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.models.ProductDetails;

import java.util.ArrayList;
import java.util.List;

public class OrderItemMapper {

    private OrderItemMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static List<Product> toProducts(List<OrderItem> source) {
        List<Product> target = new ArrayList<>();
        for (OrderItem item : source) {
            target.add(mapItemToProduct(item));
        }
        return target;
    }

    private static Product mapItemToProduct(OrderItem item) {
        Product product = new Product();
        product.setId(item.getId());
        product.setQuantity(item.getQuantity());
        product.setDetails(new ProductDetails());
        if (item.getPrice() != null)
            product.getDetails().setPrice(item.getPrice());
        if (item.getName() != null)
            product.setName(item.getName());
        return product;
    }

    public static List<OrderItem> toOrderItems(List<Product> source) {
        List<OrderItem> target = new ArrayList<>();
        for (Product product : source) {
            target.add(mapProductToItem(product));
        }
        return target;
    }

    public static OrderItem mapProductToItem(Product product) {
        OrderItem item = new OrderItem();
        item.setId(product.getId());
        item.setQuantity(product.getQuantity());
        item.setPrice(product.getDetails().getPrice());
        item.setName(product.getName());
        item.setStock(product.getStock());
        return item;
    }

}
