package fr.epsi.rennes.mspr.tpre814.orders.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderItemMapper;
import fr.epsi.rennes.mspr.tpre814.orders.utils.OrderMapper;
import fr.epsi.rennes.mspr.tpre814.shared.events.*;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.utils.ItemMapper;
import jakarta.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.naming.ServiceUnavailableException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

import static fr.epsi.rennes.mspr.tpre814.config.KafkaConfig.*;
import static fr.epsi.rennes.mspr.tpre814.shared.enums.OrderStatus.READY;
import static org.apache.kafka.common.utils.Utils.sleep;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaService {
    private static final ObjectMapper mapper = new ObjectMapper();
    final ConcurrentHashMap<UUID, CompletableFuture<BaseEvent>> pendingRequests = new ConcurrentHashMap<>();
    private final KafkaTemplate<UUID, String> anyTemplate;


    @SneakyThrows
    @PreDestroy
    void tearDown() {
        sleep(300);
        log.info("Order Service stopped, awaiting for pending orders");
        pendingRequests.forEach((k, v) ->
                v.completeExceptionally(
                        new ServiceUnavailableException(
                                "Service is shutting down")));
    }

    private CompletableFuture<BaseEvent> newRequest(UUID id) {
        CompletableFuture<BaseEvent> fut = new CompletableFuture<>();
        pendingRequests.put(id, fut);
        return fut;
    }

    /**
     * Get the pending request
     *
     * @param id the id of the request
     * @return the request
     * @throws IllegalStateException if the request is not found
     */
    CompletableFuture<BaseEvent> getPending(UUID id) {
        CompletableFuture<BaseEvent> fut = pendingRequests.get(id);
        if (fut == null) {
            throw new IllegalStateException("No pending request for " + id);
        }
        return fut;
    }

    @Async
    public CompletableFuture<BaseEvent> requestCustomer(UUID customerId, UUID orderId) {
        log.info("Requesting customer {}", customerId);
        CompletableFuture<BaseEvent> fut = newRequest(customerId);
        CustomerRequest request = new CustomerRequest(customerId, orderId);
        log.debug("CUSTOMER JSON REQUEST :" + request.toJson());
        anyTemplate.send(CUSTOMER_REQUEST, customerId, request.toJson());
        return fut;
    }

    @Async
    public CompletableFuture<BaseEvent> reserveProducts(Order newOrder) {
        log.info("Requesting stock subtraction for items {}", newOrder.getProducts());
        CompletableFuture<BaseEvent> fut = newRequest(newOrder.getId());
        StockRequest stockRequest = new StockRequest(newOrder.getId(), ItemMapper.toProductRequest(OrderItemMapper.toProducts(newOrder.getProducts())));
        stockRequest.setOp(StockOperation.SUB);
        log.debug("RESERVE JSON REQUEST :" + stockRequest.toJson());
        anyTemplate.send(RESERVE_TOPIC, newOrder.getId(), stockRequest.toJson());
        return fut;
    }

    //##############################################################################################################
    //################################################ LISTENERS ##################################################
    //##############################################################################################################

    /**
     * Await for kafka reponses
     *
     * @param response the Customer to listenCustomer, received from the customer service
     */
    @KafkaListener(topics = CUSTOMER_RESPONSE, groupId = ORDER_SERVICE + "-2")
    public void listenCustomer(@RequestBody String response,
                               @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("Received Customer => {}", response);
        CustomerResponse responseEvent;
        responseEvent = mapper.readValue(response, CustomerResponse.class);
        log.info("Customer response received for {}", key);
        getPending(responseEvent.getId()).complete(responseEvent);
    }

    /**
     * Await for kafka reponses
     *
     * @param event the products to listenCustomer, received from the stock service
     */
    @KafkaListener(topics = STOCK_RESPONSE, groupId = ORDER_SERVICE + "-1")
    public void listenStock(String event,
                            @Header(KafkaHeaders.RECEIVED_KEY) UUID key) throws JsonProcessingException {
        log.info("Received Stock => {}", event);
        StockResponse stockEvent;
        stockEvent = mapper.readValue(event, StockResponse.class);
        log.info("Stock response received for {}", key);
        getPending(stockEvent.getId()).complete(stockEvent);
    }

    /**
     * Get the response from the pending requests
     *
     * @param id the id of the request
     * @return the response
     * @throws Exception if the request is not found or if the response is not received in time
     */
    @SneakyThrows
    public BaseEvent getResponse(UUID id) {
        return getPending(id).get(10, java.util.concurrent.TimeUnit.SECONDS);
    }

    /**
     * Aggregate the results of requested services
     * /!!\ Must be used AFTER the kafka calls are done and joined !
     *
     * @param initOrder the order to update
     * @return the updated order to be saved
     * If anything went poorly but not critically, the resulting order will be returned
     * with param `warn` containing the error messages
     */
    public Order aggregateResults(Order initOrder) {
        Customer cust = ((CustomerResponse) getResponse(initOrder.getCustomerId())).getPayload();
        StockResponse stock = ((StockResponse) getResponse(initOrder.getId()));

        OrderMapper.updateCustomer(initOrder, cust);
        OrderMapper.updateOrderProductsFromStockEvent(initOrder, stock);
        initOrder.setStatus(READY);

        return initOrder;
    }
}