package fr.epsi.rennes.mspr.tpre814.orders.api.interfaces;

import java.util.List;

public interface CRUDInterface<T, K, R> {

    R create(T entity);

    T get(K id);

    List<T> all();

    T update(T entity);

    void delete(K id);

}
