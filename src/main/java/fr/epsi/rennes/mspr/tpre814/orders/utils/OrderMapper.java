package fr.epsi.rennes.mspr.tpre814.orders.utils;

import fr.epsi.rennes.mspr.tpre814.orders.database.entity.Order;
import fr.epsi.rennes.mspr.tpre814.orders.database.entity.OrderItem;
import fr.epsi.rennes.mspr.tpre814.shared.events.StockResponse;
import fr.epsi.rennes.mspr.tpre814.shared.models.Customer;
import fr.epsi.rennes.mspr.tpre814.shared.models.OrderDTO;
import fr.epsi.rennes.mspr.tpre814.shared.models.Product;
import fr.epsi.rennes.mspr.tpre814.shared.utils.ModelMapper;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;


@Slf4j
public class OrderMapper implements ModelMapper {

    private OrderMapper() {
        throw new IllegalStateException("Utility class");
    }

    public static Order updateOrder(@NonNull Order dbOrder, @NonNull Order entity) {
        log.info("Updating order {}\n with {}", dbOrder.getId(), entity);
        if (entity.getCustomerId() != null) {
            dbOrder.setCustomerId(entity.getCustomerId());
        }
        if (entity.getDeliveryAddress() != null) {
            dbOrder.setDeliveryAddress(entity.getDeliveryAddress());
        }
//        if (!(entity.getProducts() == null || entity.getProducts().isEmpty())) {
//            dbOrder = updateOrderProducts(dbOrder, entity.getProducts());
//        }
        if (entity.getTotalPrice() > 0.0) {
            dbOrder.setTotalPrice(entity.getTotalPrice());
        }
        if (entity.getStatus() != null) {
            dbOrder.setStatus(entity.getStatus());
        }
        if (entity.getWarn() != null) {
            dbOrder.setWarn(entity.getWarn());
        }
        dbOrder.setUpdatedAt(LocalDateTime.now());
        return dbOrder;
    }


    /**
     * Pour chaque produit récupéré du stock,
     * vérifie si le produit est valide (stock > 0) et met à jour le prix de la ligne et le total de la commande
     *
     * @param order         commande à mettre à jour
     * @param stockResponse liste des produits récupérés du stock
     */
    public static void updateOrderProductsFromStockEvent(Order order, StockResponse stockResponse) {
        AtomicReference<Double> totalPrice = new AtomicReference<>(0.0);

        order.getProducts().forEach(orderItem -> {
            Product stockProduct = findStockProduct(stockResponse, orderItem.getId());

            if (stockProduct == null || stockProduct.getStock() == 0) {
                log.warn("Product {} not in stock", orderItem.getId());
                return;
            }

            updateOrderItem(order, orderItem, stockProduct, totalPrice);
        });

        order.setTotalPrice(totalPrice.get());
        log.info("Total price of the order : {}", order.getTotalPrice());
        log.debug(order.toString());
    }

    private static Product findStockProduct(StockResponse stockResponse, UUID productId) {
        return stockResponse.getPayload().stream()
                .filter(p -> p.getId().equals(productId))
                .findFirst()
                .orElse(null);
    }

    private static void updateOrderItem(Order order, OrderItem orderItem, Product stockProduct, AtomicReference<Double> totalPrice) {
        log.info("Requested {} VS Stock {}", orderItem.getQuantity(), stockProduct.getStock());

        orderItem.setPrice(stockProduct.getDetails().getPrice());
        orderItem.setName(stockProduct.getName());

        if (stockProduct.getStock() >= orderItem.getQuantity()) {
            log.info("Product {} is in stock for quantity : {}", stockProduct.getId(), orderItem.getQuantity());
            totalPrice.updateAndGet(v -> v + orderItem.getPrice() * orderItem.getQuantity());
        } else {
            handleStockShortage(order, orderItem, stockProduct, totalPrice);
        }
    }

    private static void handleStockShortage(Order order, OrderItem orderItem, Product stockProduct, AtomicReference<Double> totalPrice) {
        String warningMessage = String.format("Item %s : %s requested but only %s available",
                orderItem.getId(), orderItem.getQuantity(), stockProduct.getStock());

        order.setWarn(warningMessage);
        log.warn(warningMessage);

        totalPrice.updateAndGet(v -> v + orderItem.getPrice() * stockProduct.getStock());
        orderItem.setQuantity(stockProduct.getStock());
    }

    public static void updateCustomer(Order newOrder, Customer customer) {
        log.info("Updating customer for order {}", newOrder.getId());
        newOrder.setCustomerId(customer.getId());
        newOrder.setDeliveryAddress(customer.getAddress().getId());
        log.debug(newOrder.toString());
    }

    public static OrderDTO toDTO(Order order) {
        OrderDTO ret = new OrderDTO();
        ret.setId(order.getId());
        ret.setCustomerId(order.getCustomerId());
        ret.setAddressId(order.getDeliveryAddress());
        ret.setProducts(OrderItemMapper.toProducts(order.getProducts()));
        ret.setTotalPrice(order.getTotalPrice());
        ret.setStatus(order.getStatus());
        ret.setWarn(order.getWarn());
        return ret;
    }
}

